from __future__ import division

import json
import string
import time
from collections import defaultdict
from queue import Queue, Empty

from google.cloud import speech_v1p1beta1
from google.cloud.speech_v1p1beta1 import enums
from google.cloud.speech_v1p1beta1 import types
import pyaudio
from six.moves import queue
import asyncio
import websockets
from dostoevsky.tokenization import RegexTokenizer
from dostoevsky.models import FastTextSocialNetworkModel
import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import RussianStemmer, SnowballStemmer

# Audio recording parameters
RATE = 48000
CHUNK = int(RATE / 10)  # 100ms

# pacmd load-module module-loopback latency_msec=5
# https://stackoverflow.com/a/56612274/6219719


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


def get_sentiment_model():
    tokenizer = RegexTokenizer()
    return FastTextSocialNetworkModel(tokenizer=tokenizer)


def get_max_sentiment(model, text):
    try:
        sentiments = model.predict([text.lower()])[0]
        return sorted(sentiments.items(), key=lambda x: x[1], reverse=True)[0][0]
    except IndexError:
        return 'unknown'


async def start(websocket, cmd_queue):
    # See http://g.co/cloud/speech/docs/languages
    # for a list of supported languages.
    language_code = 'ru-RU'  # a BCP-47 language tag

    client = speech_v1p1beta1.SpeechClient()
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=RATE,
        language_code=language_code,
        enable_word_time_offsets=True,
        enable_speaker_diarization=True,
        diarization_speaker_count=2,
    )
    streaming_config = types.StreamingRecognitionConfig(
        config=config,
        interim_results=False)

    sentiment_model = get_sentiment_model()

    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()
        requests = (types.StreamingRecognizeRequest(audio_content=content)
                    for content in audio_generator)

        responses = client.streaming_recognize(streaming_config, requests)

        last_timestamp = None
        for response in responses:
            if not response.results or not response.results[0].is_final:
                continue

            speaker_to_message = defaultdict(list)

            # The `results` list is consecutive. For streaming, we only care about
            # the first result being considered, since once it's `is_final`, it
            # moves on to considering the next utterance.
            result = response.results[0]
            if not result.alternatives:
                continue

            start_time = None
            for word in result.alternatives[0].words:
                end_time = word.end_time.seconds + word.end_time.nanos * 1E-9
                # print(f'word: {word.word}, tag: {word.speaker_tag}, end_time: {end_time}')

                if not last_timestamp or end_time > last_timestamp:
                    if not start_time:
                        start_time = int((time.time() + word.end_time.seconds + word.end_time.nanos * 1E-9) * 1000)
                    speaker_to_message[word.speaker_tag].append(word.word)
                    last_timestamp = end_time

            if result.is_final and speaker_to_message:
                res = []
                for tag, words in speaker_to_message.items():
                    message = " ".join(words)
                    sentiment = get_max_sentiment(sentiment_model, message)
                    res.append({
                        'start_time': start_time,
                        'user': tag,
                        'text': message,
                        'speachColor': sentiment,
                        'tasks': get_tasks_from_text(message)
                    })

                if res:
                    asyncio.ensure_future(send(websocket, res))
            await asyncio.sleep(0.5)

            try:
                cmd = cmd_queue.get_nowait()
                print(cmd)
                if cmd == 'stop':
                    return
            except Empty:
                pass


async def send(websocket, data):
    print(data)
    await websocket.send(json.dumps(data))


async def echo(websocket, path):
    print('echo')
    cmd_queue = Queue()
    async for message in websocket:
        if message == 'start':
            asyncio.ensure_future(start(websocket, cmd_queue))
        else:
            print(f'received: {message}')
            cmd_queue.put(message)


def main():
    start_server = websockets.serve(echo, "0.0.0.0", 8765)

    try:
        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
        pass


def prepare_nltk():
    nltk.download("stopwords")
    nltk.download('punkt')
    nltk.download('averaged_perceptron_tagger')

    nltk.download('maxent_ne_chunker')
    nltk.download('words')


def prepare_tokens_and_tags(text):
    tokens = nltk.word_tokenize(text.lower())
    tokens = [i for i in tokens if (i not in string.punctuation)]
    stop_words = stopwords.words('russian')
    tokens = [i for i in tokens if (i not in stop_words)]
    stemmer = SnowballStemmer('russian')
    tokens = [stemmer.stem(i) for i in tokens]

    tagged = nltk.pos_tag(tokens)
    return {x[1]: x[0] for x in tagged}


def find_tasks_in_tags(tags_to_tokens):
    res = []
    if any(x in tags_to_tokens.values() for x in ['встрет', 'встреч', 'встреча', 'встретьт']) and 'CD' in tags_to_tokens:
        res.append({
            'task_type': 'meeting',
            'task_time': tags_to_tokens['CD']
        })
    if any(x in tags_to_tokens.values() for x in ['созвон', 'позвон']) and 'CD' in tags_to_tokens:
        res.append({
            'task_type': 'call',
            'task_time': tags_to_tokens['CD']
        })
    return res


def get_tasks_from_text(text):
    try:
        tags_to_tokens = prepare_tokens_and_tags(text)
        return find_tasks_in_tags(tags_to_tokens)
    except LookupError:
        prepare_nltk()
        return get_tasks_from_text(text)


if __name__ == '__main__':
    main()
